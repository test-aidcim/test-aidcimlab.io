# Non-profit organization website
 __aidCIM.org__

***

## Latest build: [Gitlab pages](https://dzinemon.gitlab.io/churchbtstrp) 

## Forked project with custom domain: [Aid Christian International Ministry](http://www.aidcim.org/) 

***

a template made using 
[Bootstrap](http://getbootstrap.com/)
and 
[Jekyll](https://jekyllrb.com/)

***

images from 
[Pexels](https://www.pexels.com/) 
& 
[Placeholdit](https://placehold.it/)

icons
[Fontawesome](http://fontawesome.io/)